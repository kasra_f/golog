# GoLog
A simple logging tool for GoLang

# TODO
  * Feature: Setting the log level in the application scope
  * Feature: Align messages based on max file name so far.
  * Bug    : Print package/method instead of file name.
